﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using goappbackend.Models;

namespace goappbackend.Data
{
    public class goappbackendContext : DbContext
    {
        public goappbackendContext (DbContextOptions<goappbackendContext> options)
            : base(options)
        {
        }

        public DbSet<goappbackend.Models.Test> Test { get; set; }

        public DbSet<goappbackend.Models.TestResult> TestResult { get; set; }
    }
}
