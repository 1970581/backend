﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using goappbackend.Data;
using goappbackend.Services;

namespace goappbackend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // CORS
            services.AddCors(o => o.AddPolicy("PolicyAll", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "goappbackend", Version = "v1" });
            });


            // DOESNT WORK IN UBUNTO, HARCODING IN THE APPSETTINGS.
            // USER NAME AND PASSWORD for gonogo data database comes from enviorement variables.
            //string db_user = Environment.GetEnvironmentVariable("GO_BACKEND_DB_USER");
            //string db_pass = Environment.GetEnvironmentVariable("GO_BACKEND_DB_PASS");

            //string myConnectionString = Configuration.GetConnectionString("goappbackendContext");
            //myConnectionString = myConnectionString.Replace("MY_PASSWORD", db_pass);
            //myConnectionString = myConnectionString.Replace("MY_USERNAME", db_user);
            ///Console.WriteLine(myConnectionString);

            string myConnectionString = Configuration.GetConnectionString("goappbackendContext");

            services.AddDbContext<goappbackendContext>(options =>
                    options.UseSqlServer(myConnectionString));

            services.AddSingleton<IKeycloakService,KeycloakService>();
            services.AddSingleton<IPredictionService, PredictionService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "goappbackend v1"));
            }

            app.UseHttpsRedirection();

            app.UseCors("PolicyAll");

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
