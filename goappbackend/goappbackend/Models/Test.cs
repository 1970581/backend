﻿using System.ComponentModel.DataAnnotations;

namespace goappbackend.Models
{
    public class Test
    {
        [Key]
        public int id { get; set; }

        [MaxLength(50)]
        public string text { get; set; }
    }
}
