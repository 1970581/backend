﻿using System;
using System.ComponentModel.DataAnnotations;

namespace goappbackend.Models
{
    public class TestResult
    {
        [Key]
        public int id { get; set; }
        [Required]
        [MaxLength(50)]
        public string userId { get; set; }
        [Required]
        [MaxLength(50)]
        public string username { get; set; }
        [Required]
        [MaxLength(50)]
        public string rgpd { get; set; }
        [Required]        
        public DateTime timestamp {get;set;}
        [Required]
        public int age { get; set; }
        [Required]
        [MaxLength(50)]
        public string gender { get; set; }
        [Required]
        [MaxLength(50)]
        public string version { get; set; }
        [Required]        
        public int selfFatigueLevel { get; set; }
        [Required]        
        public bool isRated { get; set; }
        [Required]        
        public int predictedFatigueLevel { get; set; }
        [Required]
        public int gotrialnumber { get; set; }
        [Required]
        public int nogotrialnumber { get; set; }
        [Required]
        public int gofailnumber { get; set; }
        [Required]
        public int nogofailnumber { get; set; }
        [Required]
        public double gofailpercentage { get; set; }
        [Required]
        public double nogofailpercentage { get; set; }
        [Required]
        public double goavg { get; set; }

    }
}
