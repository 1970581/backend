﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goappbackend.Models
{
    /// <summary>
    /// Class for the IDEPA test of the front end. Not for actual use.
    /// </summary>
    public class GecadTest
    {
        public int fatigueLevel { get; set; }
        public string ip { get; set; }
    }
}
