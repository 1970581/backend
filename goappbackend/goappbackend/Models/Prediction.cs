﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goappbackend.Models
{
    public class Prediction
    {
        public int age { get; set; }        
        public string gender { get; set; }        
        public int goavg { get; set; }        
        public int gofailnumber { get; set; }
        public int nogofailnumber { get; set; }
        
        public int predictedFatigueLevel { get; set; }

    }
}
