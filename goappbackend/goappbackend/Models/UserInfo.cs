﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goappbackend.Models
{
    public class UserInfo
    {
        public string sub { get; set; }
        public string birthdate { get; set; }
        public string gender { get; set; }
        public string lingua { get; set; }
        public string preferred_username { get; set; }
        public string locale { get; set; }
        public string rgpd { get; set; }
    }
}
