﻿using goappbackend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goappbackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GecadController : ControllerBase
    {

        
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<GecadTest>> PostTestResult(GecadTest test)
        {
            await Task.CompletedTask;
            return Ok(test);
            //return CreatedAtAction("GetTestResult", new { id = testResult.id }, testResult);
        }
    }
}
