﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using goappbackend.Data;
using goappbackend.Models;
using goappbackend.Services;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace goappbackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestResultsController : ControllerBase
    {

        public static string HEADER_USERNAME = "username";
        public static string HEADER_TOKEN = "token";

        private readonly goappbackendContext _context;
        private readonly IKeycloakService _keycloakService;
        private readonly IPredictionService _predictionService;
        private readonly IConfiguration _config;
        private static readonly HttpClient httpClient = new HttpClient();

        public TestResultsController(goappbackendContext context, IKeycloakService keycloakService, IPredictionService predictionService, IConfiguration config)
        {
            _context = context;
            _keycloakService = keycloakService;
            _predictionService = predictionService;
            _config = config;
        }

        /*
        // GET: api/TestResults
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TestResult>>> GetTestResult()
        {            
            return await _context.TestResult.ToListAsync();
        }
        */

        // GET: api/TestResults/5
        [HttpGet("{id}")]
        public async Task<ActionResult<List<TestResult>>> GetTestResult(int id, [FromHeader] GoHeaders goHeaders)
        {
            string token = goHeaders?.token ?? String.Empty;
            string username = goHeaders?.username ?? String.Empty;

            bool isAuthorised = await _keycloakService.validateUser(token, username);

            if (!isAuthorised) return Unauthorized();


            //List<TestResult> results = await _context.TestResult.FindAsync

            int days = id;
            if (days < 1) days = 1;
            DateTime now = DateTime.UtcNow;
            DateTime older = now.AddDays(-1 * days);

            List<TestResult> results = _context.TestResult.Where(p => username.Equals(p.username) && p.timestamp <= now && p.timestamp >= older).ToList();

            //var testResult = await _context.TestResult.FindAsync(id);
            //if (testResult == null)
            //{
            //    return NotFound();
            //}

            return results;
        }
        

        // POST: api/TestResults
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TestResult>> PostTestResult(TestResult testResult, [FromHeader] GoHeaders goHeaders)
        {
            string token = goHeaders?.token ?? String.Empty;
            string username = goHeaders?.username ?? String.Empty;

            bool isAuthorised = await _keycloakService.validateUser(token, username);

            if (!isAuthorised) return Unauthorized();

            // Get a prediction:
            int fatigueLevel = -1;
            fatigueLevel = await _predictionService.predictFatigue(testResult);
            testResult.predictedFatigueLevel = fatigueLevel;
            if (fatigueLevel > 0)
            {
                testResult.isRated = true;
            }


            _context.TestResult.Add(testResult);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTestResult", new { id = testResult.id }, testResult);
        }

        // Metodo para fazer de proxy.
        // POST: api/TestResults/fatigue2a
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("fadiga2a")]
        public async Task<ActionResult<Prediction>> PostProxyFatigue2a(Prediction ask) 
        {
            string url = _config.GetConnectionString("prediction_server_url_2a");
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            string my_body = JsonConvert.SerializeObject(ask);
            httpRequestMessage.Content = new StringContent(my_body, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.SendAsync(httpRequestMessage);
            string json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Prediction>(json);
        }

        /*
        [HttpPost("fadiga2aa")]
        public RedirectResult PostProxyFatigue2aa()
        {
            string url = _config.GetConnectionString("prediction_server_url_2a");
            return new RedirectResult(url);
        }
        */

        /*
        // DELETE: api/TestResults/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTestResult(int id)
        {
            var testResult = await _context.TestResult.FindAsync(id);
            if (testResult == null)
            {
                return NotFound();
            }

            _context.TestResult.Remove(testResult);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        */

        /*
        private bool TestResultExists(int id)
        {
            return _context.TestResult.Any(e => e.id == id);
        }
        */
    }

    public class GoHeaders {
        [FromHeader]
        public string username { get; set; }
        [FromHeader]
        public string token { get; set; }
    }
}
