﻿using goappbackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Text;

namespace goappbackend.Services
{
    public class PredictionService : IPredictionService
    {
        private static readonly HttpClient httpClient = new HttpClient();
        private readonly IConfiguration _config;

        public PredictionService(IConfiguration config)
        {
            _config = config;
        }

        public async Task<int> predictFatigue(TestResult testResult)
        {
            Prediction ask = new Prediction();
            ask.age = testResult.age;
            ask.gender = testResult.gender;
            ask.gofailnumber = testResult.gofailnumber;
            ask.nogofailnumber = testResult.nogofailnumber;
            ask.goavg = (int) testResult.goavg;


            string url = _config.GetConnectionString("prediction_server_url_3");
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url);

            string my_body = JsonConvert.SerializeObject(ask);
            httpRequestMessage.Content = new StringContent(my_body, Encoding.UTF8, "application/json");


            HttpResponseMessage response = await httpClient.SendAsync(httpRequestMessage);

            if (!response.IsSuccessStatusCode)
            {
                return -1;
            }
            else
            {
                string json = await response.Content.ReadAsStringAsync();

                Prediction prediction = null;
                try
                {
                    prediction = JsonConvert.DeserializeObject<Prediction>(json);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return -1;
                }

                if (prediction != null )
                {
                    return prediction.predictedFatigueLevel;
                }
                else return -1;

            }
        }
    }
}
