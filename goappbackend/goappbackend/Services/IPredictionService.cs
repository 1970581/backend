﻿using goappbackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goappbackend.Services
{
    public interface IPredictionService
    {
        public Task<int> predictFatigue(TestResult testResult);
    }
}
