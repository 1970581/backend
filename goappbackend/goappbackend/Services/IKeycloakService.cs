﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goappbackend.Services
{
    public interface IKeycloakService
    {
        public Task<bool> validateUser(string token, string username);
    }
}
