﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using goappbackend.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace goappbackend.Services
{
    public class KeycloakService : IKeycloakService
    {
        private static readonly HttpClient httpClient = new HttpClient();
        private readonly IConfiguration _config;

        public KeycloakService(IConfiguration config)
        {
            _config = config;
        }

        public async Task<bool> validateUser(string token, string username)
        {
            string url = _config.GetConnectionString("keycloak_userinfo");
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, url);
            httpRequestMessage.Headers.Add("Authorization", "Bearer " + token);

            HttpResponseMessage response = await httpClient.SendAsync(httpRequestMessage);
            if (!response.IsSuccessStatusCode)
            {
                return false;
            }
            else {
                string json = await response.Content.ReadAsStringAsync();

                UserInfo userInfo = null;
                try 
                {
                    userInfo = JsonConvert.DeserializeObject<UserInfo>(json);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }

                if (userInfo != null && 
                    userInfo.preferred_username != null && 
                    userInfo.preferred_username.Equals(username, StringComparison.OrdinalIgnoreCase)) {
                    return true;
                }
                else return false;                
                
            }
            
        }
    }
}
