# README #

Este backend é o responsavel por receber o resultado de um teste Go/NoGo, confirmar a autorização, obter uma previsão do modelo, guardar informação na base de dados e devolver a informação.
Foi desenvolvido em .Net 5.0.


O ficheiro backend.txt contem informação de como instalar o .Net num servidor Ubunto.

O diretorio servico contem informação do serviço SystemD a usar. Basta fazer copy paste.


## Serviços:

GET: https://gonogoapp.org:5001/api/Tests        
Permite verificar se a base da dados se encontra online. Convem ter uma entrada na tabela Test.

POST: https://gonogoapp.org:5001/api/TestResults      
Envia o resultado do teste para guardar e obter previsão. Requere autorização.

POST: https://gonogoapp.org:5001/api/TestResults/fadiga2a           
Pede um previsão, servindo o backend apenas de redirecionamento do pedido.

GET: https://gonogoapp.org:5001/api/TestResults/15              
Pede todas as entradas de fadiga até a 15 dias no passado. Em vez de 15 pode-se usar o valor de dias pretendido. Requer autorização.

O servidor ao correr localmente no visual studio gera um Swagger automaticamente que pode ser usado para ver o formato dos pedidos. Uma copia desse swagger encontra-se dentro do diretorio Swagger.

### Autorização:
Enviada sob a forma de duas headers:
````
KEY : VALUE
username : a1
token : valor_do_token_keycloak
````

### Parametros:

O ficheiro appsettings.json contem as configurações do Backend. Nomeadamente:
 * - A connection string de acesso a base de dados
 * - O url de acesso ao Keycloack para confirmar a autorização
 * - O url do modelo de previsão de 3 classes
 * - O url do modelo de previsão de 2 classes alternativas
 * - Localização do certificado e chave para o HTTPS

## Testes

O diretorio testes contem testes de integração para todos os seviços.
Para o Keycloak. Para este Backend. E para o Modelo.
Sob a forma de uma coleção de testes Postman.

## Base de dados

A base de dados é uma base de dados SQL server.
Apenas necessita da tabela TestResult para guardar os dados.
A tabela test serve para testar se o backend tem acesso a base de dados.

### Criar a tabela test.
```
IF NOT EXISTS (SELECT name FROM sys.tables where name = 'test')
BEGIN
CREATE TABLE [dbo].[test](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](50) NULL
CONSTRAINT [PK_test] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
```

### Criar a tabela test result
----------------------
```
IF NOT EXISTS (SELECT name FROM sys.tables where name = 'TestResult')
BEGIN
CREATE TABLE [dbo].[TestResult](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [nvarchar](50) NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[rgpd] [nvarchar](50) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[age] [int] NOT NULL,
	[gender] [nvarchar](50) NOT NULL,
	[version] [nvarchar](50) NOT NULL,
	[selfFatigueLevel] [int] NOT NULL,
	[isRated] [bit] NOT NULL,
	[predictedFatigueLevel] [int] NOT NULL,
	[gotrialnumber] [int] NOT NULL,
	[nogotrialnumber] [int] NOT NULL,
	[gofailnumber] [int] NOT NULL,
	[nogofailnumber] [int] NOT NULL,
	[gofailpercentage] [float] NOT NULL,
	[nogofailpercentage] [float] NOT NULL,
	[goavg] [float] NOT NULL
 CONSTRAINT [PK_TestResult] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
```