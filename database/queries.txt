
Criar a tabela test.
-------------------------------------
IF NOT EXISTS (SELECT name FROM sys.tables where name = 'test')
BEGIN
CREATE TABLE [dbo].[test](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](50) NULL
CONSTRAINT [PK_test] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END


Criar a tabela test result
----------------------

IF NOT EXISTS (SELECT name FROM sys.tables where name = 'TestResult')
BEGIN
CREATE TABLE [dbo].[TestResult](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [nvarchar](50) NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[rgpd] [nvarchar](50) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[age] [int] NOT NULL,
	[gender] [nvarchar](50) NOT NULL,
	[version] [nvarchar](50) NOT NULL,
	[selfFatigueLevel] [int] NOT NULL,
	[isRated] [bit] NOT NULL,
	[predictedFatigueLevel] [int] NOT NULL,
	[gotrialnumber] [int] NOT NULL,
	[nogotrialnumber] [int] NOT NULL,
	[gofailnumber] [int] NOT NULL,
	[nogofailnumber] [int] NOT NULL,
	[gofailpercentage] [float] NOT NULL,
	[nogofailpercentage] [float] NOT NULL,
	[goavg] [float] NOT NULL
 CONSTRAINT [PK_TestResult] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
